#include "cuStatHost.h"
#include "cuStat.h"
#include <Eigen/Dense>
////This file instantiate necessary host codes (majorly eigen related)
namespace cuStat {
    template
    void copyToEigen(Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> &eigenMatrix, const Matrix<double> &cuMat);

    template
    void copyFromEigen(const Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> &eigenMatrix, Matrix<double> &cuMat);

    template
    void copyToEigen(Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic> &eigenMatrix, const Matrix<int> &cuMat);

    template
    void copyFromEigen(const Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic> &eigenMatrix, Matrix<int> &cuMat);
}