#pragma once

#include <Eigen/Dense>
#include <cuda_runtime.h>

namespace cuStat {
    template<typename HostMat, typename DeviceMat>
    void copyFromHostMat(const HostMat &hostMat, const DeviceMat &deviceMat);

    template<typename HostMat, typename DeviceMat>
    void copyToHostMat(HostMat &hostMat, const DeviceMat &deviceMat);

    template<typename ScalarType, typename cuMatType>
    void copyToEigen(Eigen::Matrix<ScalarType, Eigen::Dynamic, Eigen::Dynamic> &eigenMatrix, const cuMatType &cuMat);

    template<typename ScalarType, typename cuMatType>
    void copyFromEigen(const Eigen::Matrix<ScalarType, Eigen::Dynamic, Eigen::Dynamic> &eigenMatrix, cuMatType &cuMat);

    template<typename ScalarType, typename cuMatType>
    void
    copyToEigenAsync(Eigen::Matrix<ScalarType, Eigen::Dynamic, Eigen::Dynamic> &eigenMatrix, const cuMatType &cuMat,
                     cudaStream_t stream);

    template<typename ScalarType, typename cuMatType>
    void
    copyFromEigenAsync(const Eigen::Matrix<ScalarType, Eigen::Dynamic, Eigen::Dynamic> &eigenMatrix, cuMatType &cuMat,
                       cudaStream_t stream);

    ////Copy a device matrix to Eigen, invert, transpose and write back to device
    template<typename cuMatType>
    void inverseWithEigenTranspose(cuMatType &cuMat, cudaStream_t stream);

    template<typename ScalarType, typename hostMatType>
    Eigen::Matrix<ScalarType, Eigen::Dynamic, Eigen::Dynamic> getEigenMat(const hostMatType &hostMat);


    template<typename Matrix>
    void standardize(Matrix &X);

    template<typename Vector>
    double variance(const Vector &v);

    template<typename Vector>
    double average(const Vector &v);

    template<typename Matrix, typename Scalar>
    void rescale(Matrix &matrix, const Scalar scale);

    template<typename Scalar, template<typename> class Host,
            template<typename> class Device>
    void copyFromHostAsync(const Host<Scalar> &host, Device<Scalar> &device, cudaStream_t stream);


    template<typename Scalar, template<typename> class Host,
            template<typename> class Device>
    void copyToHostAsync(Host<Scalar> &host, const Device<Scalar> &device, cudaStream_t stream);


    ////Implementation
    template<typename Scalar, template<typename> class Host,
            template<typename> class Device>
    void copyFromHostAsync(const Host<Scalar> &host, Device<Scalar> &device, cudaStream_t stream) {
        assert(host.rows() == device.rows());
        assert(host.cols() == device.cols());

        cudaMemcpyAsync(device.data(), host.data(), sizeof(Scalar) * host.rows() * host.cols(),
                        cudaMemcpyHostToDevice, stream);

    }

    template<typename Scalar, template<typename> class Host,
            template<typename> class Device>
    void copyToHostAsync(Host<Scalar> &host, const Device<Scalar> &device, cudaStream_t stream) {
        assert(host.rows() == device.rows());
        assert(host.cols() == device.cols());

        cudaMemcpyAsync(host.data(), device.data(), sizeof(Scalar) * host.rows() * host.cols(),
                        cudaMemcpyDeviceToHost, stream);
    }


    template<typename ScalarType, typename cuMatType>
    void copyToEigen(Eigen::Matrix<ScalarType, Eigen::Dynamic, Eigen::Dynamic> &eigenMatrix, const cuMatType &cuMat) {
        ScalarType *hostPtr = (ScalarType *) eigenMatrix.data();
        const ScalarType *devPtr = cuMat.data();

        long nRow = eigenMatrix.rows();
        long nCol = eigenMatrix.cols();

        cudaMemcpy(hostPtr, devPtr, sizeof(ScalarType) * nRow * nCol, cudaMemcpyDeviceToHost);

    }

    template<typename ScalarType, typename cuMatType>
    void copyFromEigen(const Eigen::Matrix<ScalarType, Eigen::Dynamic, Eigen::Dynamic> &eigenMatrix, cuMatType &cuMat) {
        ScalarType *hostPtr = (ScalarType *) eigenMatrix.data();
        ScalarType *devPtr = cuMat.data();

        long nRow = eigenMatrix.rows();
        long nCol = eigenMatrix.cols();

        cudaMemcpy(devPtr, hostPtr, sizeof(ScalarType) * nRow * nCol, cudaMemcpyHostToDevice);
    }

    template<typename ScalarType, typename cuMatType>
    void
    copyToEigenAsync(Eigen::Matrix<ScalarType, Eigen::Dynamic, Eigen::Dynamic> &eigenMatrix, const cuMatType &cuMat,
                     cudaStream_t stream) {
        ScalarType *hostPtr = (ScalarType *) eigenMatrix.data();
        const ScalarType *devPtr = cuMat.data();

        long nRow = eigenMatrix.rows();
        long nCol = eigenMatrix.cols();

        cudaMemcpyAsync(hostPtr, devPtr, sizeof(ScalarType) * nRow * nCol, cudaMemcpyDeviceToHost, stream);
    };

    template<typename ScalarType, typename cuMatType>
    void
    copyFromEigenAsync(const Eigen::Matrix<ScalarType, Eigen::Dynamic, Eigen::Dynamic> &eigenMatrix, cuMatType &cuMat,
                       cudaStream_t stream) {
        ScalarType *hostPtr = (ScalarType *) eigenMatrix.data();
        ScalarType *devPtr = cuMat.data();

        long nRow = eigenMatrix.rows();
        long nCol = eigenMatrix.cols();

        cudaMemcpyAsync(devPtr, hostPtr, sizeof(ScalarType) * nRow * nCol, cudaMemcpyHostToDevice, stream);
    }

    template<typename cuMatType>
    void inverseWithEigenTranspose(cuMatType &cuMat, cudaStream_t stream) {
        assert(cuMat.rows() == cuMat.cols());
        using type = typename cuMatType::type;
        Eigen::Matrix<type, Eigen::Dynamic, Eigen::Dynamic> hostMat = Eigen::Matrix<type, Eigen::Dynamic, Eigen::Dynamic>::Constant(
                cuMat.rows(),
                cuMat.cols(),
                0);
        copyToEigenAsync(hostMat, cuMat, stream);
        cudaStreamSynchronize(stream);
        hostMat = hostMat.transpose().inverse();
        copyFromEigenAsync(hostMat, cuMat, stream);
    }

    template<typename ScalarType, typename hostMatType>
    Eigen::Matrix<ScalarType, Eigen::Dynamic, Eigen::Dynamic> getEigenMat(const hostMatType &hostMat) {
        Eigen::Matrix<ScalarType, Eigen::Dynamic, Eigen::Dynamic> EigenMat(hostMat.rows(), hostMat.cols());

        for (size_t i = 0; i < hostMat.rows(); i++) {
            for (size_t j = 0; j < hostMat.cols(); j++) {
                EigenMat(i, j) = (ScalarType) hostMat(i, j);
            }
        }
        return EigenMat;
    }

    template<typename Matrix>
    void standardize(Matrix &X) {
        for (int col = 0; col < X.cols(); col++) {
            double mean = average(X.col(col));
            double std = sqrt(variance(X.col(col)));
            for (int row = 0; row < X.rows(); row++) {
                X(row, col) = (X(row, col) - mean) / std;
            }
        }
    }

    template<typename Vector>
    double variance(const Vector &v) {
        double result = 0.0;
        double mean = average(v);
        for (int i = 0; i < v.rows(); i++) {
            result += (v(i) - mean) * (v(i) - mean);
        }

        return result / v.rows();
    }

    template<typename Vector>
    double average(const Vector &v) {
        double result = 0.0;
        for (int i = 0; i < v.rows(); i++) {
            result += v(i);
        }
        return result / v.rows();
    }

    template<typename Matrix, typename Scalar>
    void rescale(Matrix &matrix, const Scalar scale) {
        for (int i = 0; i < matrix.rows(); i++) {
            for (int j = 0; j < matrix.cols(); j++) {
                matrix(i, j) = matrix(i, j) * scale;
            }
        }
    }
}