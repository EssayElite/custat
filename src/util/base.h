#pragma once
#include "managed.h"
namespace cuStat{
    namespace internal{
        template<typename Scalar, typename Derived>
        class Base : public Managed{
        public:
        };

        template<typename Scalar, typename LHS, typename RHS>
        class AddOp: Base<Scalar, AddOp<Scalar, LHS, RHS>>{
        public:
        };

        template<typename Scalar>
        class Assignable{
        public:

        };
    }
}